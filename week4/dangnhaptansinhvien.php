<?php
    $phankhoa = array(" "=>" ","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học dữ liệu");
    $gender = array('0' => "Nam ", " ", '1' => "Nữ ");
    $myemail = "comments@myemail.com";
    $errors  = array();
    $values  = array();
    $errmsg  = '';
    date_default_timezone_set('Asia/Calcutta');
           $date = new DateTime();
           $new_date = $date->format('d/m/Y');
           // echo $new_date;
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        foreach($_POST as $key => $value) {
            $values[$key] = trim(stripslashes($value)); // basic input filter
        }
        if (check_input($values['yourname']) == false) { 
            $errors['yourname'] = 'Hãy nhập tên!';
        }
        //  if (check_input($values['gender']) == false) { 
        //     $errors['gender'] = 'Hãy gioi tính!';
        // }
        if(!isset($_POST['gender'])) {
           // echo '<p class="notification">Hãy chọn giới tính</p>';
           $errors['gender'] = 'Hãy chọn giới tính!';
        } 
        //  if (check_input($values['phankhoa']) == false) { 
        //     $errors['phankhoa'] = 'Hãy nhập tên!';
        // }
        // if(!isset($_POST['phankhoar'])) {
        //    // echo '<p class="notification">Hãy chọn giới tính</p>';
        //    $errors['phankhoa'] = 'Hãy chọn phân khoa';
        // } 
         if($_POST['department'] =='') {
                   // echo '<p class="notification">Hãy chọn phân khoa</p>';
                $errors['department'] = 'Hãy chọn phân khoa!';
                }


        // if(!isset($_POST['phankhoa'])== "") {
        //    // echo '<p class="notification">Hãy chọn giới tính</p>';
        //    $errors['phankhoa'] = 'Hãy chọn phân khoa';
        // } 


        if($_POST['date']== "") {
           // echo '<p class="notification">Hãy chọn giới tính</p>';
           $errors['date'] = 'Hãy nhập ngày';
        } 
      if (check_input($values['diachi']) == false) { 
            $errors['diachi'] = 'Hãy nhập địa chỉ!';
        }


    if (sizeof($errors) == 0) {
        // you can process your for here and redirect or show a success message
        $values = array(); // empty values array
        echo "Form was OK!  Good to process...<br />";
    } else {
        // one or more errors
        foreach($errors as $error) {
            $errmsg .= $error . '<br />';
        }
    }
    }
    function check_input($input) {
    if (strlen($input) == 0) {
        return false;
    } else {
        // TODO: other checks?

        return true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
    .establish {
        display: flex;
        Alight-item:center;
        line-height: 30px;
        margin-top: 8px;
    }
    .box_label {
        max-width: 130px;
         background-color: rgb(102, 143, 255);
        margin-right: 30px;
        color: #fff;border: 2px solid #0b67ad;
    }
    .form {
        line-height: 30px;
        margin-top: 8px;
    } 
    </style>
</head>
<body>
    <center>
    <div class="Box-border" 
    style = "border: 2px solid rgb(26, 50, 255);
        background-color: white;
        padding: 50px 40px 50px 50px;
        position: absolute;
        margin-left: 25%;
        margin-top: 2%;
        width: 40%;
    ">
    <div>
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
            <?php if ($errmsg != ''): ?>
            <p style="color: red;"><b>Xin vui lòng sửa những lỗi sau:</b><br />
                <?php echo $errmsg; ?>
            </p>
             <?php endif; ?>
            <div class="establish ">
                <label  class = "box_label"style="background-color:rgb(102, 143, 255);
                    padding-right: 30px;
                    padding-left: 30px;
                    color :white;
                    border:2px solid #0b67ad" for="username">Họ và tên<span class = "iconred" style=" color: orange;">*</span></label>
                <input style="flex: 2; border: 2px solid #0b67ad;" 
                    name="yourname" value="<?php echo htmlspecialchars(@$values['yourname']) ?>"
                />
            </div>

            <div class="form" style=" display: flex;">
                <label class="box_label" style="
                    padding-right: 30px;
                    padding-left: 35px;
                "> Giới tính<span class = "iconred" style=" color: orange;">*</span></label>
                <div>
                    <?php            
 //echo htmlspecialchars(@$values['genders']);
                        for($i=0; $i< sizeof($gender); $i++ ) {
                            echo '<input style="background: #20b835" type="radio" name ="gender"  '.'<label
                            style="margin-left: 6px">'.$gender[$i].'</label>';
                        }
                        ?>
                </div>
            </div>
              <div class= "form" style="display: flex;">
                <label class="box_label" style="
                    padding-right: 30px;
                    padding-left: 25px;
                "> Phân khoa<span class = "iconred" style=" color: orange;">*</span></label>
                <select  name="department" value="<?php echo htmlspecialchars(@$values['phankhoa']) ?> style="border: 2px solid #0b67ad;width: 150px;" >
                    <?php  
                        foreach($phankhoa as $key => $value) {
                        echo '<option name = "phankhoa">'.$value.'</option>';
                        }
                    ?>
                </select>
            </div>
           <div class="establish">
                    <label class="box_label" style="flex: 1">Ngày sinh <span class = "iconred" style=" color: orange;">*</span></label>
                    <!-- <input  
                    <?php
                    echo htmlspecialchars(@$values['date']);
                       
                    ?>
                    style=" border: 2px solid #0b67ad;width: 150px;"
                               type="date" name="from"
                    > -->
                    <input class="datepicker" name="date" placeholder="dd/mm/yyyy"
                        style="border: 2px solid #0b67ad; width: 137px">
                </div>
          <div class="establish">
                    <label class="box_label" style="flex: 1">Địa chỉ</label>
                    <input   name="diachi"
                    <?php
                    echo htmlspecialchars(@$values['diachi']);
                       
                    ?>
                    style="flex: 2; border: 2px solid #0b67ad;">
                </div>
        <input 
             style="margin-left: 0%;
             margin-top: 5%  ;height: 44px; width: 120px; border-radius: 6px; border: 2px solid rgb(51, 51, 255); background-color: rgb(0, 0, 204); color: #fff; font-size: 15px;"
             type="submit" value="ĐĂNG KÝ"
             >
        </input>
    </div>
        </form>      
        </div>
    </center>
</body>

