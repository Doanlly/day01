<?php 
    $errors  = array();
    $values  = array();
    $date = new DateTime();
    $new_date = $date->format('d/m/Y');
    $flag = true;
    if(!empty($_POST)) {
        $question1 = $_POST['question1'];
        $question2 = $_POST['question2'];
        $question3 = $_POST['question3'];
        $question4 = $_POST['question4'];
        $question5 = $_POST['question5'];

        $arr_answer = array($question1,$question2,$question3,$question4,$question5);
     
        setcookie('arr_answer',json_encode($arr_answer),time() + (86400 * 30), "/" );
        header('Location: q_a1.php');
    }
    
    if(count($_COOKIE) > 0) {
  echo "Cookies chưa tạo.";
} else {
  echo "Cookies đã tạo.";
}

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>

    <style>
        .establish {
         display: flex;
        Alight-item:center;
        line-height: 30px;

        margin-top: 8px;
        }
        .infor_text {
            /*margin-top: 18px;*/
            width: 600px;
    }
         .space {
            width: 8rem;
        }
        .box_label {
        width: 140px;
        max-width: 130px;
         background-color: rgb(20, 143, 255);
        
        font-size: 15px;
         justify-content: center;
        color: #fff;border: 2px solid #0b67ad;
    }
   
    table, th, td {
  border: 1px solid black;
}


.quiz,
.choices {
    list-style-type: none;
   
}
.choices li {
    margin-bottom: 2px;
}

.choices label,
input[type="radio"] {
    cursor: pointer;
}
input[type="radio"] {
    margin-right: 8px;
}


    </style>
</head>
<body>
    
     <form method="post">  

        <div  style = "border: 2px solid rgb(26, 50, 255);
        background-color: white;
        padding: 60px 0px 50px 50px;
        position: absolute;
        margin-left: 20%;
        margin-top: 2rem;
        width: 45rem;
    ">
                
                
               
   <ul class="quiz">
    <li>
        <h4>1. Loài nào dưới đây thuộc nhóm "Động vật máu nóng"?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question1" value="A" /><span
                         di>Tắc què hoa </span 
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question1" value="B" /><span
                        >Ếch</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question1" value="C" /><span
                        >Cá sấu</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question1" value="D" /><span
                        >Chuột</span
                    ></label
                >
            </li>
        </ul>
    </li>
    <li>
        <h4>2. Đâu không phải là một ngôn ngữ lập trình?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question2" value="A" /><span
                        >C++</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question2" value="B" /><span
                        >JavaScript</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question2" value="C" /><span
                        >Pascal</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question2" value="D" /><span
                        >Hyperlink</span
                    ></label
                >
            </li>
        </ul>
    </li>
    <li>
        <h4>3.Cuộc gọi được thực hiện đầu tiên trên thế giới là giữa nhà phát minh Alexander Graham Bell và...?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question3" value="A" /><span
                        >Vợ của ông</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question3" value="B" /><span
                        >Thị trưởng thành phố</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question3" value="C" /><span
                        >Cục đăng ký phát minh</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question3" value="D" /><span
                        >Người trợ lý</span
                    ></label
                >
            </li>
        </ul>
    </li>
    <li>
        <h4>4. Đơn vị nào dưới đây không được dùng để đo thể tích?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question4" value="A" /><span
                        >Gallon</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question4" value="B" /><span
                        >Lít</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question4" value="C" /><span
                        >Ounce</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question4" value="D" /><span
                        >Yard</span
                    ></label
                >
            </li>
        </ul>
    </li>
    <li>
        <h4>5.Mạng toàn cầu (World Wide Web) được ra đời ở đâu?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question5" value="A" /><span
                        >Thụy sỹ</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question5" value="B" /><span
                        >Mỹ</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question5" value="C" /><span
                        >Việt Nam</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question5" value="D" /><span
                        >Tung Của</span
                    ></label
                >
            </li>
        </ul>
    </li>
    </ul>
    
    <button  class="establish box_label " style="margin-left: 80%;">Next</button>
    
   
    
            </div>
   </form>
   
</body>


</html>